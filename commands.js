jokes = require("./jokes.json")

// Base command class
class Command {
  constructor(name, cb, desc) {
    this.name = name
	this.execute = cb
	this.description = desc
  }
}

// Commands list
MembersCommands = new Map();
AdminsCommands = new Map();

function SendMsg(msg, txt){
	msg.channel.send(txt);
	
}

// GreetCommand function
function GreetCmd(msg){
	SendMsg(msg, 'Bienvenu sur le discord des Gilets Jaunes du Quebec ' + msg.author + '!');
}

// AdminsCommand function
function AdminsCmd(msg){
	membersWithRole = msg.guild.roles.get('541286487635918869').members;
	
	members = ""
	membersWithRole.forEach( function(member) {
		members += '\t'+member +'\n';
	});
	
    SendMsg(msg, 'Voici la liste des admins : \n\n' + members);
}

// CommandsCommand function
function CommandCmd(msg){
	
	commands = ""
	all_commands = new Map(MembersCommands,AdminsCommands);
	all_commands.forEach(item => commands += '\t'+ item.name +'\n');
	SendMsg(msg, 'LISTE DE COMMANDES \n\n' + commands);
}

// DescribeCommand function
function DescribeCmd(msg, params){
	
	if(params.length != 1){
		SendMsg(msg, "Arguments pour la commande invalide.\n\tUtilisation => !describe <commande>");
		return;
	}
	
	cmd = MembersCommands.get(params[0]);
	
	cmd_rights = 'Tous'
	
	if(cmd == undefined){
		cmd = AdminsCommands.get(params[0]);
		cmd_rights = 'Admins';
	}
	
	if(cmd == undefined){
		SendMsg(msg, "Commande < " + params[0] + " > n'existe pas.");
	}else{
		SendMsg(msg, "Commande < " + params[0] + " > \n\n\t- Qui? : "+cmd_rights+ "\n\t- Quoi? : " + cmd.description);
	}
	
}

function JokeCmd(msg){
	rd = Math.floor((Math.random() * jokes.jokes.length))
	joke = jokes.jokes[rd];
	SendMsg(msg, joke);
}

GreetCommand = new Command('greet', GreetCmd, 'Souhaite la bienvenu au membre qui lance cette commande.');
AdminsCommand = new Command('admins', AdminsCmd, 'Donne la liste des admins du serveur.');
CommandCommand = new Command('commands', CommandCmd, 'Donne la liste des commandes du bot.');
DescribeCommand = new Command('describe', DescribeCmd, "Affiche la description d'une commande.");
JokeCommand = new Command('tellajoke', JokeCmd, "Raconte une blague au hasard.");

MembersCommands.set(GreetCommand.name, GreetCommand);
MembersCommands.set(AdminsCommand.name, AdminsCommand);
MembersCommands.set(CommandCommand.name, CommandCommand);
MembersCommands.set(DescribeCommand.name, DescribeCommand);
MembersCommands.set(JokeCommand.name, JokeCommand);

/*
 [
	'promote',
	'demote',
	'kick',
	'ban',
	'reboot',
	'die'
]
*/


