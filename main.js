/**
 * 	A discord admin bot for the Gilets Jaunes Quebec discord
 */

// Import the discord.js module
const Discord = require('discord.js');

const client = new Discord.Client();

const commands = require("./commands.js")

function ParseCommand(command){
	spl_command = command.split(' ');
	console.log(spl_command);
	return {
		name:spl_command[0].slice(1),
		params:spl_command.splice(1)
	};
}

function SendMsg(){
	greeting_text = "```Bonjour et bienvenue sur le discord des gilets jaunes du Quebec. Afin de voir la liste des commandes disponible , veuillez ecrire : !commands ```"
	
	client.channels.get('541273299020414977').send(greeting_text);
}

/**
 * The ready event is vital, it means that only _after_ this will your bot start reacting to information
 * received from Discord
 */
client.on('ready', () => {
  console.log('I am ready!');
  SendMsg();
  setInterval(SendMsg, 300000);
});


// Create an event listener for messages
client.on('message', message => {
	if(message.content.startsWith('!')){
		
		command = ParseCommand(message.content);
		console.log('command received : ' + command.name + ' with params ' + command.params)
		cmd = MembersCommands.get(command.name);
		
		if(cmd != undefined){
			cmd.execute(message, command.params);
		}else{
			message.channel.send('unknown command');
		}
	}
});

// Log our bot in using the token from https://discordapp.com/developers/applications/me
client.login('NTQyNDM5ODc3ODE2NDgzODcz.DzuCDA.Gv87gU80E0B5VUwFWvdaTOtT9vk');